
## Description

Gaviti challenge app

## Installation without docker

```bash
$ npm install
```

```bash
$ npm run start:dev gaviti
$ npm run start:dev customer
$ npm run start:dev etl
```

## Installation WITH docker

```bash
$ docker-compose up -d
```

## Running the app

Application is running on port 3000 (main API) and 3001(etl service), customer service is internal TCP microservice

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

# TODO
1. Dockerize