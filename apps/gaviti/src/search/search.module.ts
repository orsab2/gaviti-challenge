import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';
import { CUSTOMER_MICROSERVICE } from '../constants/customer.micro';

@Module({
  imports:[
    ClientsModule.register([
      { name: CUSTOMER_MICROSERVICE, transport: Transport.TCP },
    ]),
  ],
  controllers: [SearchController],
  providers: [SearchService]
})
export class SearchModule {}
