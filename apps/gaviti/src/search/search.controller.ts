import { Controller, Get, Header, Inject, Query, Res } from '@nestjs/common';
import { map, Observable } from 'rxjs';
import { SearchDTO } from './dto/search.dto';
import { SearchService } from './search.service';
import { DataDocument } from '../../../etl/src/schemas/data.schema';

@Controller('search')
export class SearchController {
    constructor(
        private readonly searchService: SearchService,
    ){

    }

    @Get('/')
    @Header('Content-type', 'plain/csv')
    @Header('Content-Disposition', 'attachment; filename=export.csv')
    search(@Query() qs: SearchDTO): Observable<any>{
        return this.searchService.searchByTimeDistance(qs.startDate, qs.endDate)
        .pipe(
            map(resp => resp.reduce((acc, item) => {
                acc += `${item.createdAt},${item.customerId},${item.invoiceId}\n`
                return acc
            }, 'createdAt,customerId,invoiceId\n'))
        )
        
    }
}
