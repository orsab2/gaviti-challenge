import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { CUSTOMER_MICROSERVICE, CUSTOMER_SEARCH_BY_DISTANCE_PATTERN } from '../constants/customer.micro';
import { DataDocument } from '../../../etl/src/schemas/data.schema';

@Injectable()
export class SearchService {
    constructor(
        @Inject(CUSTOMER_MICROSERVICE) private customerService: ClientProxy,
    ){

    }
    searchByTimeDistance(startDate:number, endDate:number): Observable<DataDocument[]>{
        const result = this.customerService.send(CUSTOMER_SEARCH_BY_DISTANCE_PATTERN, {startDate, endDate})

        return result
    }
}
