import { ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";

@ValidatorConstraint()
export class IsTimestamp implements ValidatorConstraintInterface {
    validate(timestamp: string) {
        const time = Number(timestamp)
        const isValid = Boolean(time > 0 && time < (new Date('9999-12-29')).getTime())
        return isValid
    }
}