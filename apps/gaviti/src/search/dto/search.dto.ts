import { IsNumber, IsNumberString, IsPositive, IsRFC3339, Validate } from "class-validator";
import { IsTimestamp } from "../validators/timestamp.validator";

export class SearchDTO{
    @Validate(IsTimestamp, {
        message: 'startDate in wrong timestamp format!',
    })
    @IsNumberString()
    startDate: number

    @Validate(IsTimestamp, {
        message: 'endDate in wrong timestamp format!',
    })
    @IsNumberString()
    endDate: number
}