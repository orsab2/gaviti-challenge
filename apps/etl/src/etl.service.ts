import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import * as fs from 'fs'
import { Model } from 'mongoose';
import * as path from 'path'
import { DataDocument } from './schemas/data.schema';

@Injectable()
export class EtlService {
  constructor(
    @InjectModel('data') 
    private dataModel: Model<DataDocument>,
  ){

  }

  @Cron('* * * * *', {name: 'ETL function'})
  async etlData() {
    await this.dataModel.deleteMany()

    const csv = fs.readFileSync(path.resolve('./fakeData/data.csv')).toString()

    const datas = []
    const rows = csv.split("\r\n")
    for(let i=1;i<rows.length;++i){
      const [createdAt, customerId, invoiceId] = rows[i].split(',')

      if(createdAt){
        datas.push({
          createdAt:Number(createdAt), customerId, invoiceId
        })
      }
    }

    console.log(datas)
    await this.dataModel.insertMany(datas)
  }
}
