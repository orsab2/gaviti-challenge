import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type DataDocument = Data & Document;

@Schema()
export class Data {
  @Prop({type:String, required:true})
  createdAt: number;

  @Prop()
  invoiceId: string;

  @Prop()
  customerId: string;
}

export const DataSchema = SchemaFactory.createForClass(Data);