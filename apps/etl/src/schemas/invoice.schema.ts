import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type InvoceDocument = Invoce & Document;

@Schema()
export class Invoce {
  @Prop()
  createdAt: number;

}

export const InvoceSchema = SchemaFactory.createForClass(Invoce);