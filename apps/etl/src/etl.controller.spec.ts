import { Test, TestingModule } from '@nestjs/testing';
import { EtlController } from './etl.controller';
import { EtlService } from './etl.service';

describe('EtlController', () => {
  let etlController: EtlController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [EtlController],
      providers: [EtlService],
    }).compile();

    etlController = app.get<EtlController>(EtlController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(etlController.getHello()).toBe('Hello World!');
    });
  });
});
