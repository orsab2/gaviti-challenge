import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { EtlController } from './etl.controller';
import { EtlService } from './etl.service';
import { DataSchema } from './schemas/data.schema';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_CON_STRING || 'mongodb://localhost/gaviti'), 
    MongooseModule.forFeature([{ schema: DataSchema, name: 'data' }])
  ],
  controllers: [EtlController],
  providers: [EtlService],
})
export class EtlModule {}
