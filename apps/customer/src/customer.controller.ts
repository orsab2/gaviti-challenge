import { Controller, Get } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { CUSTOMER_SEARCH_BY_DISTANCE_PATTERN } from 'apps/gaviti/src/constants/customer.micro';
import { SearchDTO } from 'apps/gaviti/src/search/dto/search.dto';
import { CustomerService } from './customer.service';
import { DataDocument } from '../../etl/src/schemas/data.schema';

@Controller()
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @MessagePattern(CUSTOMER_SEARCH_BY_DISTANCE_PATTERN)
  searchByPattern(@Payload() data: SearchDTO): Promise<DataDocument[]> {
    return this.customerService.searchByDistance(data.startDate, data.endDate);
  }
}
