import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DataSchema, DataDocument } from '../../etl/src/schemas/data.schema';

@Injectable()
export class CustomerService {
  constructor(
    @InjectModel('data') 
    private dataModel: Model<DataDocument>,
  ){

  }
  async searchByDistance(fromDate: number, endDate: number): Promise<DataDocument[]> {

    console.log({fromDate, endDate})
    
    const rows = await this.dataModel.find({
      $and:[
        {createdAt:{$gte: fromDate}},
        {createdAt:{$lte: endDate}},
      ]
    })
    return rows
  }
}
