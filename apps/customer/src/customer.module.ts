import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';
import { DataSchema } from '../../etl/src/schemas/data.schema';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGO_CON_STRING || 'mongodb://localhost/gaviti'), 
    MongooseModule.forFeature([{ schema: DataSchema, name: 'data' }])
  ],
  controllers: [CustomerController],
  providers: [CustomerService],
})
export class CustomerModule {}
